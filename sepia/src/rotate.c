#include <malloc.h>
#include <stdio.h>

#include "image.h"


static uint64_t get_y(const uint64_t curr_x) {
	return curr_x;
}

static uint64_t get_x(const uint64_t curr_y, const uint64_t height) {
	return height - curr_y - 1;
}

struct image rotate(const struct image source ){
	const int64_t size = source.height * source.width;
	struct image img = image_create(source.height, source.width);

	for (int64_t i = 0; i < size; i++) {
		const uint64_t x = get_x(i / source.width, source.height);
		const uint64_t y = get_y(i % source.width);
		img.data[y * source.height + x] = source.data[i];
	}
	return img;
}

