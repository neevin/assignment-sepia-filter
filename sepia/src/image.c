#include <malloc.h>
#include <stdio.h>

#include "image.h"


void free_image_memory(struct image* img) {
	free(img->data);
}

struct image image_create(uint64_t width, uint64_t height){
	struct pixel* new = malloc(sizeof(struct pixel) * (width * height));
	return (struct image) { .width = width, .height = height, .data = new };
}

struct image* image_copy(struct image img){
	struct image* new_img = malloc(sizeof(struct image));
	*new_img = image_create(img.width, img.height);
	for(uint64_t i = 0; i < img.height*img.width; i++){
		new_img->data[i] = img.data[i];
	}
	return new_img;
}
