#include <stdint.h>

#include "bmp_io.h"
#include "image.h"


enum compression{ 
	BI_RGB = 0,
	BI_RLE8 = 1,
	BI_RLE4 = 2,
	BI_BITFIELDS = 3,
	BI_JPEG = 4,
	BI_PNG = 5,
	BI_ALPHABITFIELDS = 6,
	BI_CMYK = 11,
	BI_CMYKRLE8 = 12,
	BI_CMYKRLE4 = 13,
};

#define RD_ERROR "Error while reading: "
#define WR_ERROR "Error while writing: "

const char* reading_messages[] = {
	[READ_OK] = "Data read successfully\n",
	[READ_INVALID_SIGNATURE] = RD_ERROR "Invalid signature\n",
	[READ_INVALID_BITS] = RD_ERROR "Invalid pixel data\n",
	[READ_INVALID_BITS_OFFSET] = RD_ERROR "Invalid pixels map offset\n",
    [READ_INVALID_HEADER] = RD_ERROR "Invalid header\n"
};

const char* writing_messages[] = {
	[WRITE_OK] = "Data write successfully\n",
	[WRITE_ERROR] = WR_ERROR "Can't write in output file\n"
};

static struct bmp_header read_bmp_header(FILE* file){
    struct bmp_header header;
    const size_t numblocks_count = fread(&header, HEADER_SIZE, 1, file);

    if(numblocks_count == 1){
        return header;
    }
    return (struct bmp_header) {0};
}

static uint32_t get_padding_size(const uint32_t width){
	const uint32_t val = 4 - width * sizeof(struct pixel) % 4;
    return val == 4 ? 0 : val;
}

static struct pixel* read_pixels(FILE* file, const uint32_t width, const uint32_t height, const uint32_t padding_bytes) {
    struct pixel* pixels = malloc(sizeof(struct pixel) * width * height);
	struct pixel* curr = pixels;

	for(uint32_t i = 0; i < height; i++) {
		const size_t px_count = fread(curr, sizeof(struct pixel), width, file);
		const int fseek_success = fseek(file, padding_bytes, SEEK_CUR); // Передвигаем указатель в файле

		if (px_count != width || fseek_success != 0) {
			free(pixels);
			return NULL;
		}
		curr = curr + width; 
	}
	return pixels;
}

static enum write_status write_pixels_in_add_mode(FILE* file, struct pixel* pixels, uint32_t width, uint32_t height, uint32_t padding_bytes) {
	struct pixel* curr = pixels;
    
    for(uint32_t i = 0; i < height; i++){
		const size_t c = fwrite(curr, sizeof(struct pixel), width, file);
		const size_t p = fwrite((void*) curr, 1, padding_bytes, file);

		if (c != width || p != padding_bytes) {
			return WRITE_ERROR;
		}
		curr = curr + width;
    }

	return WRITE_OK;
}

static enum read_status validate_bmp_header(struct bmp_header const * const header) {
	if (header->bOffBits != HEADER_SIZE) {
		return READ_INVALID_HEADER;
	}
	if (header->bfType != BMP_FILE_SIGNATURE) {
		return READ_INVALID_SIGNATURE;
	}
	if (header->biBitCount != BITS_PER_PIXEL) {
		return READ_INVALID_BITS;
	}
	return READ_OK;
}

static struct bmp_header create_bmp_header(uint32_t width, uint32_t height, uint32_t padding){
    const uint32_t img_size_bytes = width * height * sizeof(struct pixel) + height * padding;

    return (struct bmp_header)
    {
        .bfType = BMP_FILE_SIGNATURE,
        .bfileSize = HEADER_SIZE + img_size_bytes,
        .bfReserved = 0,
        .bOffBits = HEADER_SIZE,
        .biSize = DIB_HEADER_SIZE,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = PLANES_NUM,
        .biBitCount = BITS_PER_PIXEL,
        .biCompression = BI_RGB,
        .biSizeImage = img_size_bytes,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0,
    };     
}

enum read_status from_bmp(FILE* in, struct image* img){
	const struct bmp_header header = read_bmp_header(in);
	const enum read_status header_validation = validate_bmp_header(&header);

	if (header_validation != READ_OK){
        return header_validation;
    }
	
	const uint32_t width = header.biWidth;
	const uint32_t height = header.biHeight;
	const uint32_t padding = get_padding_size(width);

	// Отступаем от начала файла, потому что битовая карта может лежать не сразу после заголовка
	const int any_error = fseek(in, header.bOffBits, SEEK_SET);
	if(any_error){
		return READ_INVALID_BITS_OFFSET;
	}
	
	struct pixel* pixels = read_pixels(in, width, height, padding);
	if (pixels == NULL) {
		return READ_INVALID_BITS;
	}
	
    img->height = height;
    img->width = width;
    img->data = pixels;

	return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img){
    const uint32_t width = img->width;
	const uint32_t height = img->height;
	struct pixel* px_array = img->data;
	const uint32_t padding = get_padding_size(width);
	struct bmp_header header = create_bmp_header(width, height, padding);
	
	size_t written_count = fwrite(&header, sizeof(struct bmp_header), 1, out);

	if (written_count != 1){
        return WRITE_ERROR;
    }
	enum write_status status = write_pixels_in_add_mode(out, px_array, width, height, padding);
	
	return status;
}
