#include <stdio.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <string.h>

#include "bmp_io.h"
#include "image.h"
#include "rotate.h"
#include "sepia.h"


extern const char* reading_messages[];
extern const char* writing_messages[];


static const char* wrong_args_count = "Wrong arguments count\nUse: ./image_sepia option input_file output_file\n";
static const char* incorrect_second_option = "Incorrect second option, use: c|asm|test\n";
static const char* cant_open_input = "Can't open input file\n";
static const char* cant_open_output = "Can't open output file\n";
static const char* error_while_closing_input = "Error while closing input file\n";
static const char* error_while_closing_output = "Error while closing output file\n";

static void print_err(const char* error_text){
    fprintf(stderr, "%s", error_text);
}

static void print(const char* text){
    fprintf(stdout, "%s", text);
}


int main( int argc, char** argv ) {
    // у пррограммы теперь 3 аргумента:
    // 1. c|asm|test - на си, на ассемблере (sse) и прогнать тест
    // 2. input
    // 3. output
    if (argc != 4) {
        print_err(wrong_args_count);
        return 255;
    }

    FILE* in = fopen(argv[2], "rb");

    if(in == NULL)
    {
        print_err(cant_open_input);
        return 1;
    }

    struct image img = (struct image){0};

    enum read_status status = from_bmp(in, &img);

    if(status == READ_OK){
        if(strcmp(argv[1], "c") == 0) 
        {
            print("Program will use C\n");
            sepia_c_inplace(&img);
        }
        else if(strcmp(argv[1], "asm") == 0) 
        {
            print("Program will use ASM\n");
            sepia_asm_inplace(&img);
        }
        else if(strcmp(argv[1], "test") == 0) 
        {
            print("Program will do time test\n");
            struct image* new_img = image_copy(img);

            uint64_t avg_c_time = 0, avg_asm_time = 0;
            for (int i = 0; i < 10; i++) {
                struct timeval t0, t1, t2;

                gettimeofday(&t0, 0);

                sepia_c_inplace(new_img);
                gettimeofday(&t1, 0);

                sepia_asm_inplace(new_img);
                gettimeofday(&t2, 0);

                avg_c_time += (t1.tv_sec - t0.tv_sec) * 1000000L + t1.tv_usec - t0.tv_usec;
                avg_asm_time += (t2.tv_sec - t1.tv_sec) * 1000000L + t2.tv_usec - t1.tv_usec;
            }
            free_image_memory(new_img);
            sepia_asm_inplace(&img); // И один раз делаем на ASM, чтобы output был серым

            printf("Avg time for C  : %.1f\n", avg_c_time / 10.0);
            printf("Avg time for ASM: %.1f\n", avg_asm_time / 10.0);
        }
        else{
            print_err(incorrect_second_option);

            free_image_memory(&img);
            if(fclose(in)){
                print_err(error_while_closing_input);
            }
            return 255;
        }

        FILE* out = fopen(argv[3], "wb");
        if(out == NULL)
        {
            print_err(cant_open_output);
            if(fclose(in)){
                print_err(error_while_closing_input);
            }
            return 1;
        }

        enum write_status ok = to_bmp(out, &img);

        free_image_memory(&img);

        if(ok != WRITE_OK){
            print_err(writing_messages[ok]);
            if(fclose(in)){
                print_err(error_while_closing_input);
            }
            if(fclose(out)){
                print_err(error_while_closing_output);
            }
            return 255;
        }
        else{
            print(writing_messages[ok]);
            if(fclose(out)){
                print_err(error_while_closing_output);
            }
            if(fclose(in)){
                print_err(error_while_closing_input);
            }
            return 0;
        }
    }
    else{
        print_err(reading_messages[status]);
        if(fclose(in)){
            print_err(error_while_closing_input);
        }
        return 255;
    }
}
