#ifndef SEPIA_H
#define SEPIA_H

#include "image.h"


void sepia_c_inplace(struct image*);
void sepia_asm_inplace(struct image*);

#endif