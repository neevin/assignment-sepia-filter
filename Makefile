BUILDDIR=build
SRCDIR=sepia/src
INCDIR=sepia/include
TESTDIR=test_results

CC=gcc
CFLAGS=--std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG -O3 -I$(INCDIR)

ASMC=nasm
ASMFLAGS=-felf64 -g

LINK=ld
LINKFLAGS =-lm -no-pie

all: $(BUILDDIR)/bmp_io.o $(BUILDDIR)/image.o $(BUILDDIR)/rotate.o $(BUILDDIR)/sepia.o $(BUILDDIR)/sepia_sse.o $(BUILDDIR)/main.o
	$(CC) $(LINKFLAGS) -o $(BUILDDIR)/image_sepia $^

build:
	mkdir -p $(BUILDDIR)

tests:
	mkdir -p $(TESTDIR)

$(BUILDDIR)/bmp_io.o: $(SRCDIR)/bmp_io.c build
	$(CC) -c $(CFLAGS) $(HEADDIR) $< -o $@

$(BUILDDIR)/image.o: $(SRCDIR)/image.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/rotate.o: $(SRCDIR)/rotate.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/sepia.o: $(SRCDIR)/sepia.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/main.o: $(SRCDIR)/main.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/sepia_sse.o: $(SRCDIR)/sepia_sse.asm build
	$(ASMC) $(ASMFLAGS) $< -o $@

run:
	$(BUILDDIR)/image_sepia

test: all tests
	# run C
	$(BUILDDIR)/image_sepia c input.bmp $(TESTDIR)/output_c.bmp
	# run ASM
	$(BUILDDIR)/image_sepia asm input.bmp $(TESTDIR)/output_asm.bmp
	# run timetest
	$(BUILDDIR)/image_sepia test input.bmp $(TESTDIR)/output.bmp

clean:
	rm -rf $(BUILDDIR)
	rm -rf $(TESTDIR)

.PHONY: all test run
